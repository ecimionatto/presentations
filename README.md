# Presentations
it uses [Spectacle](https://formidable.com/open-source/spectacle/)

## create new
to create a new presentation:
1. create "empty" folder
1. cd into the folder
1. run `yarn install`
1. run ` spectacle-boilerplate -m mdx`

## run Presentation
to run existing presentation:
1. cd folder
1. `yarn start`